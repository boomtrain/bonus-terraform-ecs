provider "aws" {
  profile    = "default"
  region     = "us-east-1"
}

resource "aws_ecs_cluster" "boomtrain-cluster" {
  name = "boomtrain-cluster"
}

resource "aws_elb" "ecs_elb" {
  name = "ecs_elb"
  availability_zones = ["us-east-1a"]
}

resource "aws_ecs_service" "boomtrain-service" {
  name = "boomtrain-service"
  cluster = "${ aws_ecs_cluster.boomtrain-cluster.id }"
  task_definition = "${ aws_ecs_task_definition.service.family }"
  depends_on = ["aws_ecs_task_definition.service"]
  desired_count = 1

  load_balancer = {

    container_name = "nginx_container"
    container_port = 80

  }
}

resource "aws_ecs_task_definition" "service" {
  family = "service"
  container_definitions = "${file("task_definitions_service.json")}"

}
